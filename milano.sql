/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : milano

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2019-10-12 23:22:18
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `yupe_blog_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_blog`;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post`;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post_to_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post_to_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_tag`;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_user_to_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_user_to_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_category_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_category_category`;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_category_category
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_comment_comment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_comment_comment`;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_comment_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_contentblock_content_block`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_contentblock_content_block
-- ----------------------------
INSERT INTO `yupe_contentblock_content_block` VALUES ('1', 'Телефон:', 'telefon', '1', '<a href=\"tel:+393343270713\">+39 334 327 0713</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('2', 'Соц сети', 'soc-seti', '1', '<a href=\"https://wa.me/393343270713\"><i class=\"icon icon-whatsapp\"></i></a>\r\n<a href=\"viber://chat?number=+393343270713\"><i class=\"icon icon-viber\"></i></a>\r\n<a href=\"#\"><i class=\"icon icon-telegram\"></i></a>\r\n<a href=\"#\"><i class=\"icon icon-instagram\"></i></a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('3', 'Классические экскурсии и паломничество в Милане', 'klassicheskie-ekskursii-i-palomnichestvo-v-milane', '1', 'Классические экскурсии и паломничество в Милане', 'Заголовок над формой главная страница', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('4', 'МИЛАН: ЭКСКУРСИИ ИЛИ ПАЛОМНИЧЕСТВО', 'milan-ekskursii-ili-palomnichestvo', '1', '<div class=\"excurs-pal-home box-style\">\r\n  <div class=\"excurs-pal-home__header box-style__header color-pink\">\r\n    МИЛАН: <br>ЭКСКУРСИИ ИЛИ <br>ПАЛОМНИЧЕСТВО\r\n  </div>\r\n  <div class=\"excurs-pal-home__header txt-style\">\r\n    Вы можете выбрать между классическими экскурсиями и паломничеством. <br>\r\n    Маршруты составлены для самых притязательных и любознательных туристов. <br>\r\n    Вы точно найдете для себя то, зачем приехали в Милан.    \r\n  </div>\r\n</div>', '<p>Заголовок блока на главной</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('5', 'Мы против СПАМа', 'my-protiv-spama', '1', 'Мы против СПАМа, Ваши данные остануться у меня и будут использованы для связи с Вами <br>\r\nНажимая кнопку, Вы соглашаетесь с условиями пользовательского соглашения и политики конфидециальности.', '<p>текст внизу формы в модалке</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('6', 'Введите свои данные для связи с гидом', 'vvedite-svoi-dannye-dlya-svyazi-s-gidom', '1', 'Введите свои данные <br>для связи с гидом', '<p>Заголовок в модалке</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('7', 'Скрипты в шапку', 'skripty-v-shapku', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('8', 'Скрипты в футер', 'skripty-v-futer', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('9', 'Уведомление', 'uvedomlenie', '1', 'Ваша заявка успешно отправлена. <br>В ближайщее время с Вами свяжется наш менеджер!', '<p>Уведомление после отправки заявки</p>', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('10', 'Данный сайт носит исключительно информационный характер', 'dannyy-sayt-nosit-isklyuchitelno-informacionnyy-harakter', '1', 'Данный сайт носит исключительно информационный характер. Bсе предложения, указанные на сайте, не являются ни публичной офертой, ни рекламой.', '<p>Текст в футере где копирайт</p>', null, '1');

-- ----------------------------
-- Table structure for `yupe_gallery_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_gallery`;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_gallery_image_to_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_image_to_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_image_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_image_image`;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_image_image
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_mail_mail_event`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_event`;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_event
-- ----------------------------
INSERT INTO `yupe_mail_mail_event` VALUES ('1', 'zapisatsya-na-ekskursiyu', 'Записаться на экскурсию', '');

-- ----------------------------
-- Table structure for `yupe_mail_mail_template`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_template`;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_template
-- ----------------------------
INSERT INTO `yupe_mail_mail_template` VALUES ('1', 'zapisatsya-na-ekskursiyu', '1', 'Записаться на экскурсию', '', 'site@ash-digital.ru', 'nariman-abenov@mail.ru', 'Заявка «Записаться на экскурсию» с сайта vmilanegid.ru', '<table>\r\n<tbody>\r\n<tr>\r\n	<td><strong>Имя:</strong>\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td><strong>Телефон:</strong>\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td><strong>Форма:</strong>\r\n	</td>\r\n	<td>comment\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu`;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu
-- ----------------------------
INSERT INTO `yupe_menu_menu` VALUES ('1', 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu_item`;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu_item
-- ----------------------------
INSERT INTO `yupe_menu_menu_item` VALUES ('12', '0', '1', '1', 'О себе', '/o-sebe', null, null, null, null, null, null, '0', '0', '2', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('13', '0', '1', '1', 'Паломничество', '/palomnichestvo', null, null, null, null, null, null, '0', '0', '3', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('14', '0', '1', '1', 'Экскурсии', '/ekskursii', null, null, null, null, null, null, '0', '0', '4', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('15', '0', '1', '1', 'Услуги', '/uslugi', null, null, null, null, null, null, '0', '0', '5', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('18', '0', '1', '1', 'Главная страница', '/', 'home-link', '', '', '', '', '', '', '0', '1', '1', '', '', null);

-- ----------------------------
-- Table structure for `yupe_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_migrations`;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_migrations
-- ----------------------------
INSERT INTO `yupe_migrations` VALUES ('1', 'user', 'm000000_000000_user_base', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('2', 'user', 'm131019_212911_user_tokens', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('3', 'user', 'm131025_152911_clean_user_table', '1545824979');
INSERT INTO `yupe_migrations` VALUES ('4', 'user', 'm131026_002234_prepare_hash_user_password', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('5', 'user', 'm131106_111552_user_restore_fields', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('6', 'user', 'm131121_190850_modify_tokes_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('7', 'user', 'm140812_100348_add_expire_to_token_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('8', 'user', 'm150416_113652_rename_fields', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('9', 'user', 'm151006_000000_user_add_phone', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('10', 'yupe', 'm000000_000000_yupe_base', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('11', 'yupe', 'm130527_154455_yupe_change_unique_index', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('12', 'yupe', 'm150416_125517_rename_fields', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('13', 'yupe', 'm160204_195213_change_settings_type', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('14', 'category', 'm000000_000000_category_base', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('15', 'category', 'm150415_150436_rename_fields', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('16', 'image', 'm000000_000000_image_base', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('17', 'image', 'm150226_121100_image_order', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('18', 'image', 'm150416_080008_rename_fields', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('19', 'page', 'm000000_000000_page_base', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('20', 'page', 'm130115_155600_columns_rename', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('21', 'page', 'm140115_083618_add_layout', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('22', 'page', 'm140620_072543_add_view', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('23', 'page', 'm150312_151049_change_body_type', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('24', 'page', 'm150416_101038_rename_fields', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('25', 'page', 'm180224_105407_meta_title_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('26', 'page', 'm180421_143324_update_page_meta_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('27', 'mail', 'm000000_000000_mail_base', '1545824991');
INSERT INTO `yupe_migrations` VALUES ('28', 'comment', 'm000000_000000_comment_base', '1545824992');
INSERT INTO `yupe_migrations` VALUES ('29', 'comment', 'm130704_095200_comment_nestedsets', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('30', 'comment', 'm150415_151804_rename_fields', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('31', 'notify', 'm141031_091039_add_notify_table', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('32', 'blog', 'm000000_000000_blog_base', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('33', 'blog', 'm130503_091124_BlogPostImage', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('34', 'blog', 'm130529_151602_add_post_category', '1545825002');
INSERT INTO `yupe_migrations` VALUES ('35', 'blog', 'm140226_052326_add_community_fields', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('36', 'blog', 'm140714_110238_blog_post_quote_type', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('37', 'blog', 'm150406_094809_blog_post_quote_type', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('38', 'blog', 'm150414_180119_rename_date_fields', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('39', 'blog', 'm160518_175903_alter_blog_foreign_keys', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('40', 'blog', 'm180421_143937_update_blog_meta_column', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('41', 'blog', 'm180421_143938_add_post_meta_title_column', '1545825006');
INSERT INTO `yupe_migrations` VALUES ('42', 'contentblock', 'm000000_000000_contentblock_base', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('43', 'contentblock', 'm140715_130737_add_category_id', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('44', 'contentblock', 'm150127_130425_add_status_column', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('45', 'menu', 'm000000_000000_menu_base', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('46', 'menu', 'm121220_001126_menu_test_data', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('47', 'menu', 'm160914_134555_fix_menu_item_default_values', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('48', 'menu', 'm181214_110527_menu_item_add_entity_fields', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('49', 'gallery', 'm000000_000000_gallery_base', '1545825012');
INSERT INTO `yupe_migrations` VALUES ('50', 'gallery', 'm130427_120500_gallery_creation_user', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('51', 'gallery', 'm150416_074146_rename_fields', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('52', 'gallery', 'm160514_131314_add_preview_to_gallery', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('53', 'gallery', 'm160515_123559_add_category_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('54', 'gallery', 'm160515_151348_add_position_to_gallery_image', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('55', 'gallery', 'm181224_072816_add_sort_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('56', 'news', 'm000000_000000_news_base', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('57', 'news', 'm150416_081251_rename_fields', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('58', 'news', 'm180224_105353_meta_title_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('59', 'news', 'm180421_142416_update_news_meta_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('60', 'page', 'm180421_143325_add_column_image', '1547437100');
INSERT INTO `yupe_migrations` VALUES ('61', 'rbac', 'm140115_131455_auth_item', '1547444421');
INSERT INTO `yupe_migrations` VALUES ('62', 'rbac', 'm140115_132045_auth_item_child', '1547444422');
INSERT INTO `yupe_migrations` VALUES ('63', 'rbac', 'm140115_132319_auth_item_assign', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('64', 'rbac', 'm140702_230000_initial_role_data', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('65', 'slider', 'm000000_000000_slider_base', '1570544693');
INSERT INTO `yupe_migrations` VALUES ('66', 'page', 'm180421_143326_add_column_body_Short', '1570552413');
INSERT INTO `yupe_migrations` VALUES ('67', 'page', 'm180421_143327_add_column_column_time_price', '1570714740');
INSERT INTO `yupe_migrations` VALUES ('68', 'page', 'm180421_143328_add_page_image_tbl', '1570720225');
INSERT INTO `yupe_migrations` VALUES ('69', 'page', 'm180421_143329_add_column_column_title_children', '1570726819');

-- ----------------------------
-- Table structure for `yupe_news_news`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_news`;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_news
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_notify_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_notify_settings`;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_notify_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_page_page`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page`;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `time` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `title_children` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page
-- ----------------------------
INSERT INTO `yupe_page_page` VALUES ('1', null, 'ru', null, '2019-01-14 09:39:32', '2019-01-14 09:39:32', '1', '1', 'Главная', 'Главная', 'glavnaya', '<p>TEST</p>', '', '', '1', '0', '1', '', '', '', null, null, null, null, null, null);
INSERT INTO `yupe_page_page` VALUES ('2', null, 'ru', null, '2019-01-14 09:57:27', '2019-10-09 22:09:45', '1', '1', 'Добро пожаловать в прекрасный Милан!', 'О себе', 'o-sebe', '<p>Мне бы очень хотелось, чтобы во время моих экскурсий вам тоже понравился этот город, причем не только своим Модным кварталом, но и богатой историей.\r\n</p><p>Меня зовут Марина. Я - переводчик-референт, в Италии живу уже 25 лет, замужем за итальянцем, так что с местными традициями знакома не понаслышке! Милан похож на мой любимый Питер. Он также аристократичен и также сер и строг осенью.  У этого города особая энергетика, не случайно именно здесь создавали свои шедевры знаменитые дизайнеры.\r\n</p><p>Именно Милан дал миру Караваджо, Джо Понти, Миуччу Прада и даже… Адриано Челентано! Ну как можно не проникнуться любовью к этому городу!\r\n</p><p>Давайте вместе  разберемся, чем отличались красавицы прошлых столетий от нынешних, узнаем, почему местные мастера так не любили Леонардо да Винчи, выпьем “ каффе корретто», (то есть кофе, сдобренного местной граппой) и, поверьте,  Милан не оставит Вас равнодушным. От любой экскурсии нужно получать только приятные эмоции, а поэтому в каждом конкретном случае я сделаю ее именно «под Вас», как с точки зрения программы, так  и цены!\r\n</p><p>Если вы путешествуете с детьми, то в конце каждого «путешествия в историю» они получат заслуженный приз за внимание и выносливость!\r\n</p><p>Я верующий человек, являюсь прихожанкой одного из миланских православных приходов. Поэтому специально для православных паломников разработала программы, которые, конечно же, связаны со святителем Амвросием Медиоланским, любимым и почитаемым, как миланцами, так и всеми православными.\r\n</p><p>Если вы решите приехать приходской группой, то вместе с вашим священником мы выберем приемлемую оплату , и при этом не будем смотреть на часы.\r\n</p><p>Кроме экскурсионных программ я оказываю услуги переводчика (устный и письменный переводы (много работаю с врачами) и помогаю в заказе любого вида транспорта.\r\n</p>', '', '', '1', '0', '2', '', 'view-about', '', 'ddecb8060672c798887df29805a52c50.png', 'a649d90cff3de37f5d04312841d26dc8.jpg', '<p>Мне бы очень хотелось, чтобы во время моих экскурсий вам тоже понравился этот город, причем не только своим Модным кварталом, но и богатой историей.\r\n</p><p>Меня зовут Марина. Я - переводчик-референт, в Италии живу уже 25 лет, замужем за итальянцем, так что с местными традициями знакома не понаслышке! Милан похож на мой любимый Питер. Он также аристократичен и также сер и строг осенью.  У этого города особая энергетика, не случайно именно здесь создавали свои шедевры знаменитые дизайнеры.\r\n</p>', null, null, null);
INSERT INTO `yupe_page_page` VALUES ('3', null, 'ru', null, '2019-01-14 09:57:56', '2019-10-12 21:48:43', '1', '1', 'Паломничество', 'Паломничество', 'palomnichestvo', '<p>Земля Италийская дорога не только вековыми архитектурными сооружениями и богатым культурным наследием, но в большей мере теми христианскими святынями, чрез которые подается изобильная благодать с верою к ним притекающим.\r\n</p><p>Из наиболее важных мест, хранящих святые мощи и реликвии, можно выделить базилику покровителя города святителя Амвросия Медиоланского (IVв.), Кафедральный собор \"Дуомо\" в честь Рождества Пресвятой Богородицы, базилики св. Виктора и св. Лаврентия, св. Симплиция и св. Евсторгия.\r\n</p><div>Кроме того, в Милане есть два прихода Московского Патриархата. Один посвящен преподобным Сергию Радонежскому, Серафиму Саровскому и мученику Викентию Сарагосскому, и окормляется архимандритом Димитрием (Фантини); другой освящен в честь святителя Амвросия Медиоланского. Настоятелем храма является архимандрит Амвросий (Макар).\r\n\r\n<p>Земля Италийская дорога не только вековыми архитектурными сооружениями и богатым культурным наследием, но в большей мере теми христианскими святынями, чрез которые подается изобильная благодать с верою к ним притекающим.\r\n</p>\r\n<p>Из наиболее важных мест, хранящих святые мощи и реликвии, можно выделить базилику покровителя города святителя Амвросия Медиоланского (IVв.), Кафедральный собор \"Дуомо\" в честь Рождества Пресвятой Богородицы, базилики св. Виктора и св. Лаврентия, св. Симплиция и св. Евсторгия.\r\n</p>\r\n<p>Кроме того, в Милане есть два прихода Московского Патриархата. Один посвящен преподобным Сергию Радонежскому, Серафиму Саровскому и мученику Викентию Сарагосскому, и окормляется архимандритом Димитрием (Фантини); другой освящен в честь святителя Амвросия Медиоланского. Настоятелем храма является архимандрит Амвросий (Макар).\r\n</p></div>', '', '', '1', '0', '3', '', 'view-palom', '', null, '1ac04bc0b00ec83821c259447937ddb3.jpg', '<p>Земля Италии хранит великое наследие древней Церкви, она слышала проповеди святых первоверховных апостолов Петра и Павла, она является сокровищницей святынь, вывезенных сюда во время Крестовых походов.</p>', '', '', 'Актуальные маршруты:');
INSERT INTO `yupe_page_page` VALUES ('4', null, 'ru', null, '2019-01-14 09:58:14', '2019-10-12 22:33:47', '1', '1', 'Классические экскурсии', 'Экскурсии', 'ekskursii', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n</p>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?<br>\r\n</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n</p>', '', '', '1', '0', '4', '', 'view-excursion', '', null, '858f5630986489300a1cb2353eccd794.jpg', '<p>Что вы знаете о Милане? Что это город, в галереях которого хранятся полотна прославленных мастеров: Леонардо, Рафаэля, Браманте, Караваджо? Да, конечно! Но не только. На моих экскурсиях  мы с вами разберемся, чем отличались красавицы прошлых столетий от нынешних...</p>', '', '', 'Актуальные маршруты:');
INSERT INTO `yupe_page_page` VALUES ('5', null, 'ru', null, '2019-01-14 09:58:29', '2019-10-10 19:41:24', '1', '1', 'Услуги', 'Услуги', 'uslugi', '<p>Кроме экскурсионных программ я оказываю также услуги переводчика (устный и письменный переводы с / на итальянский) и помогаю в заказе любого вида транспорта — от автобусов «Gran Turismo» до легковых автомобилей эконом — класса и класса люкс. Кроме этого тут я размещю ссылки на своих партнеров и коллег, надеюсь они будут полезны Вам или просто интересны.</p>', '', '', '1', '0', '5', '', 'view-services', '', null, null, '', null, null, null);
INSERT INTO `yupe_page_page` VALUES ('8', null, 'ru', '5', '2019-10-09 22:56:06', '2019-10-10 22:01:10', '1', '1', 'Аренда авто', 'Аренда авто', 'arenda-avto', '<h2>Аренда авто</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.<span class=\"redactor-invisible-space\"><br></span></p>', '', '', '1', '0', '8', '', '', '', null, '5179d94cd094e3d761e204dcaf5ac5a5.jpg', '', '', '', null);
INSERT INTO `yupe_page_page` VALUES ('9', null, 'ru', '5', '2019-10-09 22:56:52', '2019-10-10 22:03:42', '1', '1', 'Устный и письменный перевод', 'Устный и письменный перевод', 'ustnyy-i-pismennyy-perevod', '<h2>Устный и письменный перевод</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.\r\n</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.\r\n</p>', '', '', '1', '0', '9', '', '', '', null, '13a15cd350b76825a65586c6a8f893cf.jpg', '', '', '', null);
INSERT INTO `yupe_page_page` VALUES ('10', null, 'ru', '5', '2019-10-10 19:40:14', '2019-10-10 21:05:05', '1', '1', 'Аренда авто', 'Аренда авто', 'arenda-avto2', '<h2>Аренда авто2</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.\r\n</p>', '', '', '1', '0', '10', '', '', '', '', '0c2cb9ef383c06966cdbf49735f4dbaa.jpg', '', '', '', null);
INSERT INTO `yupe_page_page` VALUES ('11', null, 'ru', '5', '2019-10-10 19:40:47', '2019-10-10 21:06:03', '1', '1', 'Устный и письменный перевод', 'Устный и письменный перевод', 'ustnyy-i-pismennyy-perevod2', '<h2>Устный и письменный перевод2</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.\r\n</p>', '', '', '1', '0', '11', '', '', '', '', 'bd8b5fc6e3b1c9b9f07ecaa06f08c64d.jpg', '', '', '', null);
INSERT INTO `yupe_page_page` VALUES ('12', null, 'ru', '5', '2019-10-10 19:41:16', '2019-10-10 21:12:50', '1', '1', 'Аренда авто', 'Аренда авто', 'arenda-avto3', '<h2>Аренда авто3</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur consequuntur debitis enim laboriosam nihil incidunt in reiciendis vero deserunt. Consequatur atque voluptate deleniti rerum dolor aliquid, distinctio voluptates, assumenda doloremque.\r\n</p>', '', '', '1', '0', '12', '', '', '', '', 'cc1ed890b279caa70f3aca6d4dc579fb.jpg', '', '', '', null);
INSERT INTO `yupe_page_page` VALUES ('13', null, 'ru', '3', '2019-10-10 22:44:57', '2019-10-12 21:51:27', '1', '1', 'Святитель Амвросий и его служение', 'Святитель Амвросий и его служение', 'svyatitel-amvrosiy-i-ego-sluzhenie', '<h2>Святитель Амвросий и его служение</h2><p>Базилика св. Симплиция, базилики св. Амвросия и св. муч. Виктора, Кафедральный собор «Дуомо» с посещением крестильни св.Амвросия.\r\n</p><p>Паломничество начинается от базилики Сант Амброджио, где почивают мощи святого покровителя Милана и братьев близнецов Гервасия и Протасия. В базилике св.Виктора мы сможем поклониться мощам местночтимого мученика – воина Виктора Лодийского (около 303 года). Затем, в Кафедральном соборе, мы сможем приложиться к честной главе святой первомученицы, равноапостольной Феклы, поклониться Святому Гвоздю и увидеть крестильню, где был крещен святитель Амвросий. Заканчивается паломничество в базилике в честь духовного наставника св.Амвросия – св. Симплиция, в которой мы сможем помолится также святым мученикам Сизинию, Мартирию и Александру (IV в)\r\n</p>', '', '', '1', '0', '13', '', '', '', null, '91f6467d4f389b0fc47b46a0caff91b1.jpg', '', '3 часа', '150', '');
INSERT INTO `yupe_page_page` VALUES ('14', null, 'ru', '3', '2019-10-10 22:45:40', '2019-10-11 21:26:03', '1', '1', 'Милан первых веков христианства', 'Милан первых веков христианства', 'milan-pervyh-vekov-hristianstva', '<h2>Святитель Амвросий и его служение</h2><p>Базилика св. Симплиция, базилики св. Амвросия и св. муч. Виктора, Кафедральный собор «Дуомо» с посещением крестильни св.Амвросия.\r\n</p><p>Паломничество начинается от базилики Сант Амброджио, где почивают мощи святого покровителя Милана и братьев близнецов Гервасия и Протасия. В базилике св.Виктора мы сможем поклониться мощам местночтимого мученика – воина Виктора Лодийского (около 303 года). Затем, в Кафедральном соборе, мы сможем приложиться к честной главе святой первомученицы, равноапостольной Феклы, поклониться Святому Гвоздю и увидеть крестильню, где был крещен святитель Амвросий. Заканчивается паломничество в базилике в честь духовного наставника св.Амвросия – св. Симплиция, в которой мы сможем помолится также святым мученикам Сизинию, Мартирию и Александру (IV в)\r\n</p>', '', '', '1', '0', '14', '', '', '', null, '97f54cf9203e7dad7b73a1bca521cb58.jpg', '', '3 часа', '150', '');
INSERT INTO `yupe_page_page` VALUES ('15', null, 'ru', '3', '2019-10-10 22:46:14', '2019-10-11 21:25:56', '1', '1', 'Миланские святые', 'Миланские святые', 'milanskie-svyatye', '<h2>Святитель Амвросий и его служение</h2><p>Базилика св. Симплиция, базилики св. Амвросия и св. муч. Виктора, Кафедральный собор «Дуомо» с посещением крестильни св.Амвросия.\r\n</p><p>Паломничество начинается от базилики Сант Амброджио, где почивают мощи святого покровителя Милана и братьев близнецов Гервасия и Протасия. В базилике св.Виктора мы сможем поклониться мощам местночтимого мученика – воина Виктора Лодийского (около 303 года). Затем, в Кафедральном соборе, мы сможем приложиться к честной главе святой первомученицы, равноапостольной Феклы, поклониться Святому Гвоздю и увидеть крестильню, где был крещен святитель Амвросий. Заканчивается паломничество в базилике в честь духовного наставника св.Амвросия – св. Симплиция, в которой мы сможем помолится также святым мученикам Сизинию, Мартирию и Александру (IV в)\r\n</p>', '', '', '1', '0', '15', '', '', '', null, '3118431edc46837cf41c8c37eaed8548.jpg', '', '3 часа', '150', '');
INSERT INTO `yupe_page_page` VALUES ('16', null, 'ru', '3', '2019-10-10 22:49:01', '2019-10-11 21:25:52', '1', '1', 'Святитель Амвросий и его служение', 'Святитель Амвросий и его служение', 'svyatitel-amvrosiy-i-ego-sluzhenie2', '<h2>Святитель Амвросий и его служение</h2><p>Базилика св. Симплиция, базилики св. Амвросия и св. муч. Виктора, Кафедральный собор «Дуомо» с посещением крестильни св.Амвросия.\r\n</p><p>Паломничество начинается от базилики Сант Амброджио, где почивают мощи святого покровителя Милана и братьев близнецов Гервасия и Протасия. В базилике св.Виктора мы сможем поклониться мощам местночтимого мученика – воина Виктора Лодийского (около 303 года). Затем, в Кафедральном соборе, мы сможем приложиться к честной главе святой первомученицы, равноапостольной Феклы, поклониться Святому Гвоздю и увидеть крестильню, где был крещен святитель Амвросий. Заканчивается паломничество в базилике в честь духовного наставника св.Амвросия – св. Симплиция, в которой мы сможем помолится также святым мученикам Сизинию, Мартирию и Александру (IV в)\r\n</p>', '', '', '1', '0', '16', '', '', '', null, '69eb32c024d1a0cdbbcc0088f74b72ac.jpg', '', '3 часа', '150', '');
INSERT INTO `yupe_page_page` VALUES ('17', null, 'ru', '3', '2019-10-11 21:16:35', '2019-10-11 21:27:29', '1', '1', 'Святитель Амвросий и его служение', 'Святитель Амвросий и его служение', 'svyatitel-amvrosiy-i-ego-sluzhenie4', '<h2>Святитель Амвросий и его служение</h2><p>Базилика св. Симплиция, базилики св. Амвросия и св. муч. Виктора, Кафедральный собор «Дуомо» с посещением крестильни св.Амвросия.\r\n</p><p>Паломничество начинается от базилики Сант Амброджио, где почивают мощи святого покровителя Милана и братьев близнецов Гервасия и Протасия. В базилике св.Виктора мы сможем поклониться мощам местночтимого мученика – воина Виктора Лодийского (около 303 года). Затем, в Кафедральном соборе, мы сможем приложиться к честной главе святой первомученицы, равноапостольной Феклы, поклониться Святому Гвоздю и увидеть крестильню, где был крещен святитель Амвросий. Заканчивается паломничество в базилике в честь духовного наставника св.Амвросия – св. Симплиция, в которой мы сможем помолится также святым мученикам Сизинию, Мартирию и Александру (IV в)\r\n</p>', '', '', '1', '0', '17', '', '', '', null, 'dccdbf7aebdf4daa2ff7f5f7ccbc8410.jpg', '', '', '', '');

-- ----------------------------
-- Table structure for `yupe_page_page_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page_image`;
CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page_image
-- ----------------------------
INSERT INTO `yupe_page_page_image` VALUES ('5', '8', 'b198d01ca7e956ad640bd802c766f26a.jpg', '', '', '1');
INSERT INTO `yupe_page_page_image` VALUES ('9', '8', '42444a034a9f2a40d4595c87782fb652.jpg', '', '', '5');
INSERT INTO `yupe_page_page_image` VALUES ('11', '8', '925e00b61e56167371fba4e049c0bddd.jpg', '', '', '6');
INSERT INTO `yupe_page_page_image` VALUES ('12', '9', 'd0b0197226e55dee2dd15bfc0142b44a.jpg', '', '', '7');
INSERT INTO `yupe_page_page_image` VALUES ('13', '9', 'a921f0778e404a69a51fa0937a34e9af.jpg', '', '', '8');
INSERT INTO `yupe_page_page_image` VALUES ('14', '9', '6a7ab59381c8025042802beab33ed2f3.jpg', '', '', '9');
INSERT INTO `yupe_page_page_image` VALUES ('15', '3', '6a3a7cce95a1322f2df0356af133c8ec.jpg', '', '', '11');
INSERT INTO `yupe_page_page_image` VALUES ('16', '3', '7b3a87e53bfa6bf100c48eae01c62013.jpg', '', '', '10');
INSERT INTO `yupe_page_page_image` VALUES ('17', '3', 'f30e22a4b1b43ae6d2f769afecdc6bec.jpg', '', '', '12');
INSERT INTO `yupe_page_page_image` VALUES ('18', '13', '00afbd239a7a53b86c244280ce6824fb.jpg', '', '', '13');
INSERT INTO `yupe_page_page_image` VALUES ('19', '13', 'e2ccb9dd86b25c810a8bc1d3a39e70db.jpg', '', '', '14');
INSERT INTO `yupe_page_page_image` VALUES ('20', '13', '7054ef3b50aeaa7b9bccab65454ff259.jpg', '', '', '15');
INSERT INTO `yupe_page_page_image` VALUES ('21', '4', '301e233decf0273fe2ab35b3d37d9558.jpg', '', '', '16');
INSERT INTO `yupe_page_page_image` VALUES ('23', '4', 'fdc29717312dcf990ff880c5dd37b285.jpg', '', '', '18');
INSERT INTO `yupe_page_page_image` VALUES ('24', '4', 'e643093ab3282ee6fa491bede193f1f7.jpg', '', '', '17');
INSERT INTO `yupe_page_page_image` VALUES ('25', '17', '39284dcb855be26391e9e9247f1d97a9.jpg', '', '', '19');
INSERT INTO `yupe_page_page_image` VALUES ('26', '17', 'b3a505e325a1ac4fbbf1212212c6290c.jpg', '', '', '20');
INSERT INTO `yupe_page_page_image` VALUES ('27', '17', '6bca6965371769edae14be7593518698.jpg', '', '', '21');
INSERT INTO `yupe_page_page_image` VALUES ('28', '3', 'bd63515fc55ab7d95e55cb320d04f4b4.jpg', '', '', '22');
INSERT INTO `yupe_page_page_image` VALUES ('29', '13', 'a4e055b8840184dc2bcd1e8c3f832a47.jpg', '', '', '23');

-- ----------------------------
-- Table structure for `yupe_slider`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider`;
CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider
-- ----------------------------
INSERT INTO `yupe_slider` VALUES ('1', 'слайд1', 'слайд1', '239d1a066416ef3912c2ac3e4fc560f1.jpg', '', '', '', '', '1', '1');

-- ----------------------------
-- Table structure for `yupe_user_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_tokens`;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_tokens
-- ----------------------------
INSERT INTO `yupe_user_tokens` VALUES ('5', '1', 'QdyeEepk8A9o8ZUJPUvM5Iv5R9~daLcF', '4', '0', '2019-10-11 20:58:26', '2019-10-11 20:58:26', '127.0.0.1', '2019-10-18 20:58:26');

-- ----------------------------
-- Table structure for `yupe_user_user`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user`;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user
-- ----------------------------
INSERT INTO `yupe_user_user` VALUES ('1', '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', '0', null, '', '', '', '1', '1', '2019-10-11 20:58:26', '2018-12-26 17:50:44', null, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', '1', null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_assignment
-- ----------------------------
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin', '1', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item
-- ----------------------------
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin', '2', 'Admin', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_yupe_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_yupe_settings`;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_yupe_settings
-- ----------------------------
INSERT INTO `yupe_yupe_settings` VALUES ('1', 'yupe', 'siteDescription', '', '2018-12-26 17:51:10', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('2', 'yupe', 'siteName', 'Марина Бабкина', '2018-12-26 17:51:10', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('3', 'yupe', 'siteKeyWords', '', '2018-12-26 17:51:10', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('4', 'yupe', 'email', 'nariman-abenov@mail.ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('5', 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('6', 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('7', 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('8', 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('9', 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('10', 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('11', 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('12', 'image', 'uploadPath', 'image', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('13', 'image', 'allowedExtensions', 'jpg,jpeg,png,gif', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('14', 'image', 'minSize', '0', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('15', 'image', 'maxSize', '5242880', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('16', 'image', 'mainCategory', '', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('17', 'image', 'mimeTypes', 'image/gif, image/jpeg, image/png', '2019-10-08 20:58:40', '2019-10-08 20:58:40', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('18', 'image', 'width', '1950', '2019-10-08 20:58:41', '2019-10-08 20:58:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('19', 'image', 'height', '1950', '2019-10-08 20:58:41', '2019-10-08 20:58:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('20', 'yupe', 'coreCacheTime', '3600', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('21', 'yupe', 'uploadPath', 'uploads', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('22', 'yupe', 'editor', 'redactor', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('23', 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('24', 'yupe', 'allowedIp', '', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('25', 'yupe', 'hidePanelUrls', '0', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('26', 'yupe', 'logo', 'images/logo.png', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('27', 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('28', 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('29', 'yupe', 'maxSize', '5242880', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('30', 'yupe', 'defaultImage', '/images/nophoto.jpg', '2019-10-12 22:57:42', '2019-10-12 22:57:42', '1', '1');
