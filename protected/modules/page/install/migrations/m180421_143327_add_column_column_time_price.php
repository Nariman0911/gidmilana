<?php

class m180421_143327_add_column_column_time_price extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{page_page}}', 'time', 'string');
        $this->addColumn('{{page_page}}', 'price', 'string');
	}

	public function safeDown()
	{
        $this->dropColumn('{{page_page}}', 'time');
        $this->dropColumn('{{page_page}}', 'price');
	}
}