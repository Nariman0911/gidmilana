<?php

class m180421_143329_add_column_column_title_children extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{page_page}}', 'title_children', 'string');
	}

	public function safeDown()
	{
        $this->dropColumn('{{page_page}}', 'title_children');
	}
}