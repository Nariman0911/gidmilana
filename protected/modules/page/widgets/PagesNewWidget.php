<?php
/**
 * PagesNewWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesNewWidget
 */
class PagesNewWidget extends yupe\widgets\YWidget
{
    public $id;
    public $ids = [];
    public $parent_id;
    public $limit;
    public $order = 't.position ASC';
    /**
     * @var string
     */
    public $view = 'pageswidget';

    protected $pages;

    public function init()
    {
        if($this->parent_id){
            $criteria = new CDbCriteria(array(
                'condition'=>'parent_id=:parent_id',
                'params'=>array(':parent_id'=>$this->parent_id),
            ));
            $criteria->order = $this->order;
            
            if($this->limit){
                $criteria->limit = $this->limit;
            }

            $this->pages = Page::model()->published()->findAll($criteria);
        } elseif($this->ids) {
            $criteria = new CDbCriteria();
            $criteria->order = $this->order;
            
            if($this->limit){
                $criteria->limit = $this->limit;
            }
            if(!is_array($this->ids)){
                $this->ids = explode(",", $this->ids);
            }
            $criteria->addInCondition('t.id', $this->ids);
            $this->pages = Page::model()->published()->findAll($criteria);
        } elseif($this->id) {
            $this->pages = Page::model()->published()->findByPk($this->id);
        }
        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view,[
            'pages' => $this->pages,
        ]);
    }
}
