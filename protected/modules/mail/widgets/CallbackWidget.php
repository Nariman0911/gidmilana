<?php
/**
 * CallbackWidget виджет формы на главной "Остались вопросы? Задайте их нам"
 */
Yii::import('application.modules.mail.models.form.CallbackForm');

class CallbackWidget extends yupe\widgets\YWidget
{
    public $view = 'callback-widget';

    public function run()
    {
        $model = new CallbackForm;
        if (isset($_POST['CallbackForm'])) {
            $model->attributes = $_POST['CallbackForm'];
            if($model->verify == ''){
                if ($model->validate()) {
                    Yii::app()->user->setFlash('callback-success', Yii::t('MailModule.mail', 'Your request has been successfully sent.'));
                    Yii::app()->controller->refresh();
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
