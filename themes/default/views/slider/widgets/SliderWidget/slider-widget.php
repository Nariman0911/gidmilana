<div class="slide-box slide-box-carousel">
	<?php foreach ($models as $key => $slide): ?>
	    <div class="slide-box__item">
			<div class="slide-box__img">
	    		<?= CHtml::image($slide->getImageUrl(), ''); ?>
			</div>
	    </div>
	<?php endforeach ?>
</div>