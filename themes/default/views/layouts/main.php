<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.min.css');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/main.min.js', CClientScript::POS_END);
    // Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js');
    // Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/animated/css/animate.css');
    // Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/animated/js/wow.min.js');
    // Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.easing.min.js');
    Yii::app()->clientScript->registerMetaTag('noindex, nofollow', 'robots');
    Yii::app()->clientScript->registerMetaTag('telephone=no', 'format-detection');
    ?>

    <link href="https://fonts.googleapis.com/css?family=Caveat:400,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet preload">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet preload">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&display=swap&subset=cyrillic-ext,latin-ext" rel="stylesheet">

    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    
    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 7
    ]); ?>

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--<link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>-->
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>

<div class="wrapper">
    <div class="wrap1">
        <div class="wrap-content">
        <?php $this->renderPartial('//layouts/_header'); ?>

        <?= $this->decodeWidgets($content); ?>
        </div>
    </div>
    <div class="wrap2">
        <?php $this->renderPartial('//layouts/_footer'); ?>
    </div>
</div>

<?php $this->widget('application.modules.mail.widgets.CallbackModalWidget'); ?>

<div id="messageModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Уведомление
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="modal-success-message">
                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 9
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>
<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
    'id' => 8
]); ?>
<div class='notifications top-right' id="notifications"></div>
</body>
</html>
