<footer>
    <div class="footer">
        <div class="footer__top">
            <div class="content">
                <div class="footer__item footer__item_logo">
                    <div class="footer-logo">
                         <a href="/">
                            <?= CHtml::image($this->mainAssets . '/images/footer-logo.png', ''); ?>
                            <span>Марина Бабкина</span>
                        </a>
                    </div>
                </div>
                <div class="footer__item footer__item_contact footer-contact">
                    <div class="footer-contact__item footer-contact__item_phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                    <div class="footer-contact__item footer-contact__item_soc">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 2
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bot">
            <div class="content">
                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                    'id' => 10
                ]); ?>
                <div class="footer__copy">
                    Copyright Babkina Marina <?= date('Y'); ?> &copy;
                </div>
            </div>
        </div>
    </div>
</footer>