<header class="<?= ($this->action->id=='index' && $this->id=='hp') ? 'header-home' : 'header-page'; ?>">
    <div class="content">
        <div class="header "> 
            <div class="header__logo">
                <a href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo.png', ''); ?>
                    <span>Марина Бабкина</span>
                </a>
            </div>
            <div class="header__menu header-menu">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'main', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="header__contact header-contact">
                <div class="header-contact__item header-contact__item_phone">
                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 1
                    ]); ?>
                </div>
                <div class="header-contact__item header-contact__item_soc">
                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 2
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</header>