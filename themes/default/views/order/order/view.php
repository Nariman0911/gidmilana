<?php
/* @var $model Order */
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
// Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/order-frontend.css');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store.js');

$this->title = Yii::t('OrderModule.order', 'Order #{n}', [$model->id]);
?>
<div class="page-content pay">
    <div class="content">
        <?//= Yii::app()->user->getFlash('callback-success') ?>
        <div class="">
            <h1><?= Yii::t("OrderModule.order", "Order #"); ?><?= $model->id; ?>
                <small>[<?= CHtml::encode($model->getStatusTitle()); ?>]</small>
            </h1>

            <?php
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div style="color: green;" class="flash-' . $key . '">' . $message . "</div>\n";
            }
            ?>
            <div class="pay-details">
                <div class="pay-details__items">
                    <div class="pay-details__left">
                        <?= CHtml::activeLabel($model, 'date'); ?>
                    </div>
                    <div class="pay-details__right">
                        <?= $model->date; ?>
                    </div>
                </div>
                <div class="pay-details__items">
                    <div class="pay-details__left">
                        <?= CHtml::activeLabel($model, 'delivery_id'); ?>
                    </div>
                    <div class="pay-details__right">
                        <?php if (!empty($model->delivery)) : ?>
                            <?= CHtml::encode($model->delivery->name); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="pay-details__items">
                    <div class="pay-details__left">
                        <?= CHtml::activeLabel($model, 'name'); ?>
                    </div>
                    <div class="pay-details__right">
                        <?= CHtml::encode($model->name); ?>
                    </div>
                </div>
                <div class="pay-details__items">
                    <div class="pay-details__left">
                        <?= CHtml::activeLabel($model, 'phone'); ?>
                    </div>
                    <div class="pay-details__right">
                        <?= CHtml::encode($model->phone); ?>
                    </div>
                </div>
                <div class="pay-details__items">
                    <div class="pay-details__left">
                        <?= CHtml::activeLabel($model, 'email'); ?>
                    </div>
                    <div class="pay-details__right">
                        <?= CHtml::encode($model->email); ?>
                    </div>
                </div>
                <?php if ($model->house) : ?>
                    <div class="pay-details__items">
                        <div class="pay-details__left">
                            <label><?= Yii::t("OrderModule.order", "Address"); ?></label>
                        </div>
                        <div class="pay-details__right">
                            <?php //= CHtml::encode($model->getAddress()); ?>
                            <?= $model->house; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="pay-box">
                <div class="pay-box__header">
                    <?= Yii::t("OrderModule.order", "Order details"); ?>
                </div>
                <?php foreach ((array)$model->products as $key => $position) : ?>
                    <div class="pay-box__items">
                        <div class="pay-box__name">
                            <div class="media">
                                <?php $productUrl = ProductHelper::getUrl($position->product); ?>
                                <a class="img-thumbnail pull-left" href="<?= $productUrl; ?>">
                                    <img class="media-object" src="<?= $position->product->getImageUrl(72, 72); ?>">
                                </a>

                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="<?= $productUrl; ?>"><?= CHtml::encode($position->product->name); ?></a>
                                    </div>
                                    <?php foreach ($position->variantsArray as $variant) : ?>
                                        <h6><?= $variant['attribute_title']; ?>: <?= $variant['optionValue']; ?></h6>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="pay-box__price">
                            <strong>
                                <span class=""><?= $position->price; ?></span>
                                <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?>
                                ×
                                <?= $position->quantity; ?> <?= Yii::t("OrderModule.order", "PCs"); ?>
                            </strong>
                        </div>
                        <div class="pay-box__totalPrice">
                            <strong>
                                <span><?= $position->getTotalPrice(); ?></span> <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?>
                            </strong>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="pay-bottom">
                <div class="pay-bottom__items">
                    <div class="pay-bottom__left">
                        <?= Yii::t("OrderModule.order", "Total"); ?>:
                    </div>
                    <div class="pay-bottom__right">
                        <?= $model->getTotalPrice(); ?><span class="ruble"> <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?></span>
                    </div>
                </div>
                <div class="pay-bottom__items">
                    <div class="pay-bottom__left">
                        <?= Yii::t("OrderModule.order", "Delivery price"); ?>:
                    </div>
                    <div class="pay-bottom__right">
                        <?= $model->getDeliveryPrice();?><span class="ruble"> <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?></span>
                    </div>
                </div>
                <div class="pay-bottom__items">
                    <div class="pay-bottom__left">
                        <?= Yii::t("OrderModule.order", "Total"); ?>:
                    </div>
                    <div class="pay-bottom__right">
                        <?= $model->getTotalPriceWithDelivery(); ?><span class="ruble"> <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!$model->isPaid() && !$model->isPaymentMethodSelected() && !empty($model->delivery) && $model->delivery->hasPaymentMethods()) : ?>
            <div class="pay-payments">
                <div class="pay-payments__header">
                    Способы оплаты:
                </div>
                <ul id="payment-methods">
                    <?php foreach ((array)$model->delivery->paymentMethods as $payment) : ?>
                        <li class="payment-method">
                            <div class="checkbox">
                                <input class="payment-method-radio" type="radio" name="payment_method_id"
                                       value="<?= $payment->id; ?>" checked=""
                                       id="payment-<?= $payment->id; ?>">
                                <label for="payment-<?= $payment->id; ?>">
                                    <?= CHtml::encode($payment->name); ?>
                                </label>
                                <?php if($payment->description) : ?>
                                    <div class="description">
                                        <?= $payment->description; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="payment-form hidden">
                                    <?= $payment->getPaymentForm($model); ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <div class="text-right pay-payments__button">
                    <button type="submit" class="btn but" id="start-payment">
                        <?= Yii::t("OrderModule.order", "Pay"); ?>
                        <!-- <span class="glyphicon glyphicon-play"></span> -->
                    </button>
                </div>
            </div>
        <?php else : ?>
           <div class="pay-payments">
                <p class="text-right pay-payments__button">
                    <span class="aler alert-warning" style="display: inline-block; padding: 10px;">
                        <?= $model->getPaidStatus(); ?>
                        <?= CHtml::encode($model->payment->name);?>
                    </span>
                </p>
            </div>
        <?php endif; ?>
    </div>
</div>
