<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

?>

<div class="slide-home">
	<?php $this->widget('application.modules.slider.widgets.SliderWidget'); ?>
	<?php $this->widget('application.modules.mail.widgets.CallbackWidget'); ?>
</div>

	<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
		'id' => 2,
		'view' => 'about-page'
	]); ?>

<div class="home-content">	
	<div class="content">
		<?php $this->widget('application.modules.page.widgets.ContentMyBlockWidget', [
			'id' => 4
		]); ?>
	</div>

	<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
		'id' => 4,
		'view' => 'excursions-page'
	]); ?>

	<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
		'id' => 3,
		'view' => 'palomnichestvo-page'
	]); ?>
</div>