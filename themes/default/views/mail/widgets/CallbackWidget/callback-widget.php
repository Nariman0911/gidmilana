<div class="main-form">
    <div class="content">
        <div class="main-form__header">
            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                'id' => 3,
            ]); ?>
        </div>
        <div class="main-form__body">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                'id'=>'callback-form',
                'type' => 'vertical',
                'htmlOptions' => ['class' => 'form-main', 'data-type' => 'ajax-form'],
            ]); ?>

                <?php if (Yii::app()->user->hasFlash('callback-success')): ?>
                    <script>
                        $('#messageModal').modal('show');
                        setTimeout(function(){
                            $('#messageModal').modal('hide');
                        }, 4000);
                        yaCounter57557146.reachGoal('send_order'); 
                        // gtag('event', 'send_order'); return true;
                    </script>
                <?php endif ?>
                
                <?= $form->hiddenField($model, 'verify'); ?>

                <?= $form->textFieldGroup($model, 'name', [
                    'widgetOptions'=>[
                        'htmlOptions'=>[
                            'class' => '',
                            'autocomplete' => 'off'
                        ]
                    ],
                    'prepend' => '<i class="icon icon-form-name" aria-hidden="true"></i>'
                ]); ?>
                <div class="form-group">
                    <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="icon icon-form-phone" aria-hidden="true"></i>
                        </span>
                        
                        <?php echo $form->telField($model, 'phone', [
                            'class' => 'form-control',
                            'placeholder' => 'Ваш номер телефона',
                            'autocomplete' => 'off'
                        ]); ?>
                        <?php /*$this->widget('CMaskedTextFieldPhone', [
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                                'class' => 'data-mask form-control',
                                'data-mask' => 'phone',
                                'placeholder' => 'Ваш номер телефона',
                                'autocomplete' => 'off'
                            ]
                        ])*/ ?>
                    </div>
                    <?php echo $form->error($model, 'phone'); ?>
                </div>

                <?= CHtml::submitButton(Yii::t('MailModule.mail', 'Записаться на экскурсию'), [
                    'id' => 'callback-button', 
                    'class' => 'but', 
                    'data-send'=>'ajax'
                ]) ?> 
                
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>