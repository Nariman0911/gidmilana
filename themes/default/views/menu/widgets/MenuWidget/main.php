<?php
Yii::import('application.modules.menu.components.YMenu'); ?>
<div class='menu'>
    <?php $this->widget(
         'zii.widgets.CMenu',
         [
             'encodeLabel' => false,
             'items' => $this->params['items'],
             'htmlOptions' => [
                'class' => 'menu__top',
             ],
         ]
     ); ?>
</div>

<div class="menu-fix">
    <div class="menu-fix__icon-menu">
        <div class="icon-bars">
            <div class="icon-bars__item"></div>
            <div class="icon-bars__item"></div>
            <div class="icon-bars__item"></div>
        </div>
    </div>
    <div class="menu-fix__icon-close">
        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/close.svg'); ?>
    </div>
    <div class="menu-fix__box">
        <div class="menu-fix__box2">
            <div class="menu-fix__box3">
                <?php $this->widget(
                     'zii.widgets.CMenu',
                     [
                         'encodeLabel' => false,
                         'items' => $this->params['items'],
                         'htmlOptions' => [
                            'class' => '',
                         ],
                     ]
                 ); ?>
            </div>
            <div class="menu-fix__bottom">
                <div class="header-contact menu-fix-contact">
                    <div class="header-contact__item header-contact__item_phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                    <div class="header-contact__item header-contact__item_soc">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 2
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
/*Yii::import('application.modules.menu.components.YMenu');
$this->widget(
    'bootstrap.widgets.TbNavbar',
    [
        'collapse' => true,
        'brand' => CHtml::image(
            Yii::app()->getTheme()->getAssetsUrl() . '/images/logo.png',
            Yii::app()->name,
            [
                'width' => '38',
                'height' => '38',
                'title' => Yii::app()->name,
            ]
        ),
        'brandUrl' => Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'],
        'items' => [
            CMap::mergeArray(
                [
                    'class' => 'YMenu',
                    'type' => 'navbar',
                    'items' => $this->params['items'],
                ],
                $viewParams
            ),
            [
                'class' => 'bootstrap.widgets.TbMenu',
                'type' => 'navbar',
                'items' => $this->controller->yupe->getLanguageSelectorArray(),
                'htmlOptions' => [
                    'class' => 'pull-right',
                ],
            ]
        ],
    ]
);
*/