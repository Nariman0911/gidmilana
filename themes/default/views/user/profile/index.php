<?php
$this->title = Yii::t('UserModule.user', 'Личный кабинет');
$this->breadcrumbs = [Yii::t('UserModule.user', 'Личный кабинет')];
$this->layout = "//layouts/user";

?>

<div class="lk-user">
	<div class="lk-user__img">
		<?php if($user->avatar) : ?>
            <?= CHtml::image( '/uploads/' . $this->module->avatarsDir . '/' . $user->avatar, ''); ?>
        <?php else: ?>
            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.png', '', ['class' => 'nophoto']); ?>
        <?php endif; ?>
	</div>
	<div class="lk-user__content">
		<div class="lk-user__info">
			<div class="lk-user__name">
				<?php if($user->last_name): ?>
					<div><?= $user->last_name; ?></div>
				<?php endif; ?>
				<?php if($user->first_name): ?>
					<?= $user->first_name; ?>
				<?php endif; ?>
				<?php if($user->middle_name): ?>
					<?= $user->middle_name; ?>
				<?php endif; ?>
			</div>
			<?php if($user->gender): ?>
				<div class="lk-user__gender lk-user__item">
					<span>Пол </span><?= $user->getGender(); ?>
				</div>
			<?php endif; ?>
			<?php if($user->location): ?>
				<div class="lk-user__location lk-user__item">
					<span>Адрес </span><?= $user->location; ?>
				</div>
			<?php endif; ?>
		</div>

		<div class="lk-user__contact">
			<h4>Контакты</h4>
			<?php if($user->phone): ?>
				<div class="lk-user__phone lk-user__item">
					<span>Телефон </span><?= $user->phone; ?>
				</div>
			<?php endif; ?>

			<?php if($user->email): ?>
				<div class="lk-user__email lk-user__item">
					<span>E-mail </span><?= $user->email; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	

</div>