<?php
$this->title = Yii::t('UserModule.user', 'User profile');
$this->breadcrumbs = [Yii::t('UserModule.user', 'User profile')];
$this->layout = "//layouts/user";

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'profile-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'vertical',
        'htmlOptions' => [
            'class' => 'form-white form-label',
            'enctype' => 'multipart/form-data',
        ]
    ]
);
?>
    <?= $form->errorSummary($model); ?>
    
    <h3>Настройки</h3>
    <div class="lk-setting">
        <div class="lk-setting__img">
            <div class="lk-setting__avatar">
                <?php if($user->avatar) : ?>
                    <?= CHtml::image( '/uploads/' . $this->module->avatarsDir . '/' . $user->avatar, ''); ?>
                <?php else: ?>
                    <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.png', '', ['class' => 'nophoto']); ?>
                <?php endif; ?>
            </div>
            <div class="file-upload">
                <label>
                    <?= $form->fileField($model, 'avatar'); ?>
                    <span><div id="count_file">Прикрепить файл</div></span>
                </label>
            </div>
            <script type="text/javascript">
                $(document).ready( function() {
                    $(".file-upload input[type=file]").change(function(){
                        var inputFile = document.getElementById('ProfileForm_avatar').files;
                        if(inputFile.length > 0){
                            $("#count_file").text('Выбрано файлов ' + inputFile.length);
                        }else{
                            $("#count_file").text('Прикрепить файл');
                        }
                    });
                });
            </script>
        </div>
        <div class="lk-setting__content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'last_name', [
                            'widgetOptions' => [
                                'htmlOptions'=>[
                                    'placeholder' => 'Фамилия'
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'first_name', [
                            'widgetOptions' => [
                                'htmlOptions'=>[
                                    'placeholder' => 'Имя'
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'middle_name', [
                            'widgetOptions' => [
                                'htmlOptions'=>[
                                    'placeholder' => 'Отчество'
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-group-radio">
                            <br>
                            <?= $form->labelEx($model, 'gender'); ?>
                            <div class="input-group radio-list">
                                <?= $form->radioButtonList($model, 'gender', User::model()->getGendersList(),[
                                    'template'=>'<div class="radio-inline">{input}{label}</div>',
                                    'separator' => ''
                                ]) ?>
                            </div>
                            <?= $form->error($model, 'gender');?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'phone', [
                            'widgetOptions' => [
                                'htmlOptions'=>[
                                    'class' => 'data-mask',
                                    'data-mask' => 'phone',
                                    'autocomplete' => 'off',
                                    'placeholder' => 'Телефон'
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($user, 'email', [
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'disabled' => true,
                                    'placeholder' => 'Email'
                                ],
                            ],
                        ]); ?>
                    </div>
                    <div class="col-sm-12">
                        <?= CHtml::submitButton('Сохранить данные', ['class' => 'but pull-right']) ?>
                    </div>
                </div>
            </div>
            <div class="row lk-setting__bottom">
                
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>