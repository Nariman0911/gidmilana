<?php if($pages): ?>
	<div class="content">
		<div class="palom-home box-style">
			<div class="palom-home__info">
				<div class="palom-home__header box-style__header color-blue"><?= $pages->title_short; ?></div>
				<div class="palom-home__desc txt-style"><?= $pages->body_short; ?></div>
				<div class="palom-home__but">
					<a class="but-link-svg" href="<?= Yii::app()->createUrl('/page/page/view', ['slug' => $pages->slug]); ?>">
						<span>Читать дальше</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/forward-button.svg'); ?>
					</a>
				</div>
			</div>
			<div class="palom-home__img">
				<?= CHtml::image($pages->getIconUrl(), ''); ?>
			</div>
			<div class="circle-box circle-box-1"></div>
			<div class="circle-box circle-box-3"></div>
			<div class="circle-box circle-box-4"></div>
		</div>
	</div>
<?php endif; ?>