<?php if($pages): ?>
	<div class="content">
		<div class="aboutMe-home">
			<div class="aboutMe-home__info">
				<div class="aboutMe-home__header"><?= $pages->title_short; ?></div>
				<div class="aboutMe-home__desc txt-style txt-style-small"><?= $pages->body_short; ?></div>
				<div class="aboutMe-home__but">
					<a class="but-link-svg" href="<?= Yii::app()->createUrl('/page/page/view', ['slug' => $pages->slug]); ?>">
						<span>Читать дальше</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/forward-button.svg'); ?>
					</a>
				</div>
			</div>
			<div class="aboutMe-home__img">
				<?= CHtml::image($pages->getIconUrl(), ''); ?>
			</div>
			<div class="circle-box circle-box-1"></div>
			<div class="circle-box circle-box-3"></div>
			<div class="circle-box circle-box-2"></div>
			<div class="circle-box circle-box-4"></div>
			<div class="circle-box circle-box-5"></div>
		</div>
	</div>
<?php endif; ?>