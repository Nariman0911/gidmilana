<?php if($pages): ?>
	<div class="content">
		<div class="excursions-home box-style">
			<div class="excursions-home__info">
				<div class="excursions-home__header box-style__header color-blue"><?= $pages->title_short; ?></div>
				<div class="excursions-home__desc txt-style"><?= $pages->body_short; ?></div>
				<div class="excursions-home__but">
					<a class="but-link-svg" href="<?= Yii::app()->createUrl('/page/page/view', ['slug' => $pages->slug]); ?>">
						<span>Читать дальше</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/forward-button.svg'); ?>
					</a>
				</div>
			</div>
			<div class="excursions-home__img">
				<?= CHtml::image($pages->getIconUrl(), ''); ?>
			</div>
			<div class="circle-box circle-box-4"></div>
			<div class="circle-box circle-box-2"></div>
			<div class="circle-box circle-box-1"></div>
		</div>
	</div>
<?php endif; ?>