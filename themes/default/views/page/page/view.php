<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content">
	<div class="content">
		<div class="txt-style txt-style-small">
			<h1 class="color-gradient"><?= $model->title; ?></h1>
			<?= $model->body; ?>
		</div>
	</div>
	<!-- <div class="circle-box circle-box-big"></div> -->
</div>

