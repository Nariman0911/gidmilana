<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content">
	<div class="content">
		<div class="services-view">
			<div class="services-view__info txt-style txt-style-small">
				<div class="text-back">В МИЛАНЕ</div>
				<h1 class="color-gradient"><?= $model->title_short; ?></h1>
				<?= $model->body; ?>
			</div>
		</div>
		<?php $childPages = $model->childPages(['condition' => 'childPages.status=1', 'order' =>'childPages.order ASC']); ?>
		<?php if($childPages) : ?>
			<div class="carousel-content">
				<div class="carousel-content__media">
					<div class="services-box services-box-carousel child-carousel slick-slider">
						<?php foreach ($childPages as $key => $childPage) : ?>
							<div class="services-box__item child-carousel__item">
								<a class="child-box-link" data-tab="tab" href="#child-box-<?= $key; ?>">
									<div class="services-box__name services-box__name_up color-blue-light child-carousel__name">
										<?= $childPage->title_short; ?>
									</div>
								</a>
								<div class="services-box__img child-carousel__img">
									<?= CHtml::image($childPage->getIconUrl(), ''); ?>
									<div class="child-carousel__but">
										<a class="child-box-link" class="but-border" data-tab="tab" href="#child-box-<?= $key; ?>">
											Подробнее
										</a>
									</div>
									<?php if($childPage->time || $childPage->price) : ?>
										<div class="child-carousel__attr child-carousel-attr">
											<?php if($childPage->time) : ?>
												<div class="child-carousel-attr__item">
													<i class="icon icon-clock"></i>
													<span><?= $childPage->time; ?></span>
												</div>
											<?php endif; ?>
											<?php if($childPage->price) : ?>
												<div class="child-carousel-attr__item">
													<span><?= $childPage->price; ?></span>
													<i class="icon icon-euro"></i>
												</div>
											<?php endif; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="carousel-content__nav">
					<div class="carousel-content__arrows" id="carousel-content-arrows"></div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if($childPages) : ?>
		<div class="child-box tab-content">
			<div class="content">
				<?php foreach ($childPages as $key => $childPage) : ?>
					<?php $gallery = $childPage->photos(['order' => 'position DESC']); ?>
					<div class="tab-pane fade child-box__item" id="child-box-<?= $key; ?>">
						<div class="child-box__info txt-style txt-style-small">
							<?= $childPage->body; ?>
							<div class="child-box__button">
								<a class="but" data-name="<?= $model->title . ' - ' . $childPage->title_short; ?>" data-modal="#signUpModal" href="#">Записаться на экскурсию</a>
							</div>
						</div>
						<div class="child-box__media">
							<?php if($gallery) : ?>
								<div class="child-gallery carousel-arrow-blue child-gallery-carousel slick-slider">
									<?php foreach ($gallery as $key => $image) : ?>
										<div class="child-gallery__item">
											<div class="child-gallery__img">
												<?= CHtml::image($image->getImageUrl(), $image->alt, ['title' => $image->title]); ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<!-- <div class="child-gallery-nav">
									<div class="child-gallery-nav__arrows"></div>
								</div> -->
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="circle-box circle-box-big"></div>
</div>
<?php 
$fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox',
    [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>
