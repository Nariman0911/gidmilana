<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content">
	<div class="content">
		<div class="about-view">
			<div class="about-view__img">
				<?= CHtml::image($model->getImageUrl(), ''); ?>
			</div>
			<div class="about-view__info txt-style txt-style-small">
				<div class="text-back">ГИД В МИЛАНЕ</div>
				<h1 class="color-gradient"><?= $model->title; ?></h1>
				<?= $model->body; ?>
				<div class="about-view__button">
					<a class="but" data-name="Страница - <?= $model->title; ?>" data-modal="#signUpModal" href="#">Записаться на экскурсию</a>
				</div>
			</div>
		</div>
	</div>
	<div class="circle-box circle-box-big"></div>
</div>
