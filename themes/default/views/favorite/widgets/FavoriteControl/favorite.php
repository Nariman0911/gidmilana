<?php if(false === $favorite->has($product->id)):?>
    <div class="product-favorite__button yupe-product-favorite-add" data-id="<?= $product->id;?>">
    	<i class="fa fa-star-o" aria-hidden="true"></i>
    	<span>Добавить в избранное</span>
    </div>
<?php else:?>
    <div class="product-favorite__button yupe-product-favorite-remove text-error" data-id="<?= $product->id;?>">
    	<i class="fa fa-star" aria-hidden="true"></i>
    	<span>Удалить из избранного</span>
    </div>
<?php endif;?>